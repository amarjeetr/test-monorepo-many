package pkg

func Sum(a, b int) int {
	return a + b
}

func SumMany(a ...int) int {
	var sum int
	for _, v := range a {
		sum += v
	}
	return sum
}
