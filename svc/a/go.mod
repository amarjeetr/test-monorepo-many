module gitlab.com/amarjeetr/test-monorepo-many/svc/a

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	gitlab.com/amarjeetr/test-module v1.1.0
	gitlab.com/amarjeetr/test-monorepo-many/pkg v0.0.0-20230501153327-3f305fd7acc3
)
